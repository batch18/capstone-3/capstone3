import React, {useState, useEffect, useContext, Fragment} from 'react';

import {Container, Row, Col, Jumbotron, Button, Tabs, Tab} from 'react-bootstrap';

import UserContext from './../UserContext';

import AdminView from './../components/AdminView.js';
import OrderView from './../components/OrderView.js';
import CartView from './../components/CartView.js';
import HistoryView from './../components/HistoryView.js';


export default function Profile() {

	let token = localStorage.getItem("token")

	const {user} = useContext(UserContext);
	
	const [products, setProducts] = useState([]);
	const [allOrders, setAllOrders] = useState([]);
	const [cart, setCart] = useState([]);
	const [history, setHistory] = useState([]);


	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/products/all',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setProducts(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])


	const fetchOrder = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/orders/allOrders',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setAllOrders(result)
		})
	}

	useEffect( () => {
		fetchOrder()
	}, [])
	

	const fetchCart = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/details',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setCart(result.cart)
		})
	}

	useEffect( () => {
		fetchCart()
	}, [])


	const fetchHistory = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/details',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setHistory(result.history)
		})
	}

	useEffect( () => {
		fetchHistory()
	}, [])


	let tabs = (user.isAdmin === true) ?
      <Fragment>
      	<Tabs defaultActiveKey="products" id="uncontrolled-tab-example">
        	<Tab eventKey="products" title="products">
          		<AdminView productsData={products} fetchData={fetchData}/>
        	</Tab>
        	<Tab eventKey="orderList" title="List of Orders">
          		<OrderView ordersData={allOrders} fetchData={fetchOrder}/>
        	</Tab>
       	</Tabs>
      </Fragment>
      :
      <Fragment>
      	<Tabs defaultActiveKey="cart" id="uncontrolled-tab-example">
        	<Tab eventKey="cart" title="cart">
        			<CartView cartData={cart} fetchData={fetchCart}/>
        	</Tab>
        	<Tab eventKey="history" title="Purchase History">
          		<HistoryView historyData={history} fetchData={fetchHistory}/>
        	</Tab>
        </Tabs>
      </Fragment>


	return (

		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-4 bg-dark text-white">
					  <h1>{user.lastName}, {user.firstName}</h1>
					  <h5>{user.email}</h5>
					  <h5>{user.address}</h5>
					</Jumbotron>
				</Col>
			</Row>

			
			  {tabs}
			
		</Container>
	)
}


