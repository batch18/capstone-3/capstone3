import React from 'react';

import {Container} from 'react-bootstrap'

import Banner from './../components/Banner';
import Highlights from './../components/Highlight';

export default function Home() {

	return (
		<Container fluid>
			<Banner/>
			<Highlights/>
		</Container>
		
	)
}