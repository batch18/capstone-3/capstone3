import React, {useState, useEffect, useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'

/*components*/
// import Course from './../components/Course';
import AdminView from './../components/AdminView.js';
import UserView from './../components/UserView.js';


/*mock data*/
// import courses from './../mock-data/courses';

/*context*/
import UserContext from './../UserContext';

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);
	console.log(user)

	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/products/all',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setProducts(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	// let CourseCards = courses.map( (course) => {
	// 	return <Course key={course.id} course={course}/>
	// })
 
	return(
		<Container className="p-4" fluid="true">
			{ (user.isAdmin === true) ?
					<AdminView productsData={products} fetchData={fetchData}/>
				:
					<UserView productsData={products} />
			}
		</Container>
	)


}
