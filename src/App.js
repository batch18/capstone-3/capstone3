import React, {useState, useEffect} from 'react'
import {BrowserRouter, Route, Switch } from 'react-router-dom';

// context
import UserContext from './UserContext';

// components
import AppNavbar from './components/AppNavbar';
import ErrorPage from './components/ErrorPage';
// pages
import Register from './pages/Register';
import Home from './pages/Home';
import Products from './pages/Products';
import Profile from './pages/Profile';



function App() {

  const [user, setUser] = useState(
    {
      firstName: null,
      lastName: null,
      email: null,
      address: null,
      id: null,
      isAdmin: null,
      cart: null
    }
  );

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      firstName: null,
      lastName: null,
      email: null,
      address: null,
      id: null,
      isAdmin: null,
      cart: null
    })
  }

  useEffect( () => {
    let token = localStorage.getItem('token');
    fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/details', {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${token}`
      }
    })
    .then(result => result.json())
    .then(result => {
      console.log(result) //object/ document of a user

      if(typeof result._id !== "undefined"){
        setUser({
          firstName: result.firstName,
          lastName: result.lastName,
          email: result.email,
          address: result.address,
          id: result._id,
          isAdmin: result.isAdmin,
          cart: result.cart
        })
      } else {
        setUser({
          firstName: null,
          lastName: null,
          email: null,
          address: null,
          id: null,
          isAdmin: null,
          cart: null
        })
      }
    })
  }, [])


  return (
    <UserContext.Provider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/Register" component={Register} />
          <Route exact path="/Products" component={Products} />
          <Route exact path="/Profile" component={Profile} />
          <Route component={ErrorPage} />
        </Switch>
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
