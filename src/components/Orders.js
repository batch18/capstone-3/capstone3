import React, {useState, useEffect, useContext} from 'react';

import {Container} from 'react-bootstrap'

import OrderView from './../components/OrderView.js';

import UserContext from './../UserContext';

export default function Orders(){

	const [allOrders, setAllOrders] = useState([]);

	const {user} = useContext(UserContext);


	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/orders/allOrders',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setAllOrders(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

 
	return(
		<Container className="p-4" fluid="true">
			<OrderView ordersData={allOrders} fetchData={fetchData}/>
		</Container>
	)


}
