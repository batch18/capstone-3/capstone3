import React, {useState, useEffect, Fragment} from 'react'

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Swal from 'sweetalert2';



export default function CartView(props){

	const {cartData, fetchData } = props;

	const [cart, setCart] = useState([]);



	useEffect( () => {
		const cartArr = cartData.map( (cart) => {

			return(
			<tr key={cart._id}>
				<td>{cart.itemName}</td>
				<td>{cart.description}</td>
				<td>{cart.price}</td>
			</tr>
			)
		})


		setCart(cartArr)
	}, [cartData])

	

	return(
		<Container>
			<Table>
				<thead>
					<tr>
						<th>Item name</th>
						<th>Product Description</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{/*display the courses*/}
					{cart}
				</tbody>
			</Table>

		</Container>
	)
}