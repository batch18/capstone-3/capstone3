import React from 'react'

/*react-boostrap components*/
import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function Highlights(){

	return(

		<Row className="px-3">
			<Col xs={12} md={4}>
				<Card className="my-2">
					<Card.Body>
						<Card.Title>Check out what this should have looked liked</Card.Title>
						<Card.Text>
							Im not particularly good at designing but the first iteration of this looks arguably better? well depening on who you ask
						</Card.Text>
				    	<a href="https://mishaalkallun.github.io/capstone2/pages/index.html" className="btn btn-dark">Capstone 2</a>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="my-2">
					<Card.Body>
						<Card.Title>Mass Destruction v1 </Card.Title>
						<Card.Text>
					      this is my first hosted webpage created for my guildmates at Mass Destruction in a game called SinoAlice, this was made without any proper learning about web development and design
						</Card.Text>
				    	<a href="https://mkallun.github.io/legacy" className="btn btn-dark">Check out my other works</a>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="my-2">
					<Card.Body>
						<Card.Title>Mass Destruction v2 </Card.Title>
						<Card.Text>
					      upgraded version of the v1, this time it was made with python and anvil
						</Card.Text>
				    	<a href="https://mkallun.github.io/" className="btn btn-dark">Check out my other works</a>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)}