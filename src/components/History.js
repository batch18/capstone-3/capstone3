import React, {useState, useEffect, useContext} from 'react';

import {Container} from 'react-bootstrap'

import HistoryView from './../components/HistoryView.js';

import UserContext from './../UserContext';

export default function History(){

	const [history, setHistory] = useState([]);

	const {user} = useContext(UserContext);


	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/details',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setHistory(result.history)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

 
	return(
		<Container className="p-4" fluid="true">
			<HistoryView historyData={history} fetchData={history}/>
		</Container>
	)


}
