import React from 'react';

import {Container, Row, Col, Jumbotron, Button} from 'react-bootstrap';

export default function Banner() {
	return (

		<Container fluid="true">
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-4 bg-dark text-white">
					  <h1>LaFenice's Apothecary</h1>
					  <p>
					    <a href="https://mishaalkallun.github.io/kallun-capstone1/" className="btn btn-light">Learn more about the maker</a>
					  </p>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}


