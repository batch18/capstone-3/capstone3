import React, {useState, useEffect, Fragment} from 'react'

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Swal from 'sweetalert2';



export default function HistoryView(props){

	const {historyData, fetchData } = props;

	const [history, setHistory] = useState([]);



	useEffect( () => {
		const historyArr = historyData.map( (history) => {

			return(
			<tr key={history._id}>
				<td>{history.itemName}</td>
				<td>{history.description}</td>
				<td>{history.price}</td>
			</tr>
			)
		})


		setHistory(historyArr)
	}, [historyData])

	

	return(
		<Container>
			<Table>
				<thead>
					<tr>
						<th>Item name</th>
						<th>Product Description</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{/*display the courses*/}
					{history}
				</tbody>
			</Table>

		</Container>
	)
}