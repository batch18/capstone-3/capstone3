import React, {Fragment, useContext, useState}from 'react';
import {NavLink, Link, useHistory} from 'react-router-dom'
import Swal from 'sweetalert2';

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import UserContext from './../UserContext';

import {Navbar, Nav} from 'react-bootstrap';

export default function AppNavbar(){

  let history = useHistory()
  let token = localStorage.getItem('token');

  const {user, setUser, unsetUser} = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [showLogin, setShowLogin] = useState(false);
  const openLog = () => setShowLogin(true);
  const closeLog = () => setShowLogin(false);



  const login = (e) => {
    e.preventDefault()

      fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/login', {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(result => result.json())
      .then(result => {
        console.log(result) //{access: token}

        if(typeof result.access !== "undefined"){
          //what should we do with the access token?
          localStorage.setItem('token', result.access)
          userDetails(result.access)
        }
      })

      const userDetails = (token) => {
        fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/details',{
          method: "GET",
          headers: {
            "Authorization": `Bearer ${token}`
          }
        })
        .then(result => result.json())
        .then(result => {
          console.log(result) //whole user object or document

          setUser({
            firstName: result.firstName,
            lastName: result.lastName,
            email: result.email,
            address: result.address,
            id: result._id,
            isAdmin: result.isAdmin
          });
        })
      }

      setEmail('');
      setPassword('');
      setShowLogin(false);
    }
  

  const logout = () => {
    unsetUser();
    history.push('/');
  }

  
  let leftNav = (user.id !== null) ?
    (user.isAdmin === true) ?
      <Fragment>
        <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
      </Fragment>
      :
      <Fragment>
        <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
        <Nav.Link as={NavLink} to="/products">Items</Nav.Link>
        <Nav.Link onClick={logout}>Logout</Nav.Link>

      </Fragment>
    :
    <Fragment>
      <Nav.Link as={NavLink} to="/register">Sign Up</Nav.Link>
      <Nav.Link onClick={openLog}>Sign In</Nav.Link>
    </Fragment>



  return (
    <Container fluid="true">
      <frosted-glass-container>
        <frosted-glass>
          <Navbar expand="lg">
            <Navbar.Brand as={Link} to="/">LaFenice's Apothecary</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>

              </Nav>
              <Nav>
                {leftNav}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </frosted-glass>
      </frosted-glass-container>

        <Modal show={showLogin} onHide={closeLog}>
          <Form onSubmit={(e) => login(e)}>
            <Modal.Header>
              <Modal.Title>Login</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group>
                <Form.Label></Form.Label>
                <Form.Control 
                  type="text"
                  value={email}
                  onChange={(e)=> setEmail(e.target.value)}
                  />
              </Form.Group>

              <Form.Group>
                <Form.Label></Form.Label>
                <Form.Control 
                  type="password"
                  value={password}
                  onChange={(e)=> setPassword(e.target.value)}
                  />
              </Form.Group>

            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeLog}>Close</Button>
              <Button variant="success" onClick={login}>Sign In</Button>
            </Modal.Footer>
          </Form>
        </Modal>

    </Container>


    

  )



}