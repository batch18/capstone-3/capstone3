import React, {useState, useEffect, useContext} from 'react';

import {Container} from 'react-bootstrap'

import OrderView from './../components/OrderView.js';

import UserContext from './../UserContext';

export default function Cart(){

	const [cart, setCart] = useState([]);

	const {user} = useContext(UserContext);


	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/details',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result.cart)
			setCart(result.cart)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

 
	return(
		<Container className="p-4" fluid="true">
			<CartView cartData={cart} fetchData={cart}/>
		</Container>
	)


}
