import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2';

import {Card, Button, Container, Row, Col} from 'react-bootstrap'


export default function Products({productsProp}){

	let token = localStorage.getItem("token")
	let {productName, description, price, _id, inventory, image} = productsProp
	
	if(image === ""){
		image = 'https://st4.depositphotos.com/14953852/22772/v/600/depositphotos_227725020-stock-illustration-image-available-icon-flat-vector.jpg'
	}
	
	const checkout = (e, _id) => {
		fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/checkOut',
		{
			method : "POST",	
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: _id
			})
		})
		.then( result => {
			console.log(result)
			if (result) {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Added to cart"
				})
			}
			else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	const purchase = (e, _id) => {
		fetch('https://murmuring-atoll-05490.herokuapp.com/api/users/purchase',
		{
			method : "POST",	
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: _id
			})
		})
		.then( result => {
			console.log(result)
			if (result) {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Item successfully purchased"
				})
			}
			else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	return (

		<Card className="mt-3 mx-3" border="dark" fluid="true">
			<Card.Body>
			<Row>
				<Col className="col-12 col-md-4"><Card.Img className="rounded float-left mr-3" src={image}/></Col>
				<Col>

					<Card.Title>{productName}</Card.Title>
					<Card.Text>
						<p>Description : <br/>
							{description}
						</p>
						<p>Price : <br/>
							{price}
						</p>
						<p>Remaining stocks : <br/>
							{inventory}
						</p>
						<Button className="btn btn-dark" onClick={ (e) => checkout(e, _id) }>Add to Cart</Button>
						<span>   </span>
						<Button className="btn btn-dark" onClick={ (e) => purchase(e, _id) }>Buy now</Button>
					</Card.Text>

				</Col>
			</Row>
				
				
			</Card.Body>
		</Card>



	)
}

Products.propTypes = {
	products: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
		
}