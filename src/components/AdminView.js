import React, {useState, useEffect, Fragment} from 'react'

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Swal from 'sweetalert2';

export default function AdminView(props){


	const { productsData, fetchData } = props;

	const [productId, setProductId] = useState('');
	const [products, setProducts] = useState([]);
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [image, setImage] = useState('');
	const [price, setPrice] = useState(0);
	const [inventory, setInventory] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	let token = localStorage.getItem('token');

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	useEffect( () => {
		const productsArr = productsData.map( (product) => {
			console.log(product)
			return(
			<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
						{
							(product.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>{product.inventory}</td>
					<td>
						<Fragment>
							<Button variant="primary" size="sm" 
							onClick={ ()=> openEdit(product._id) }>
								Update
							</Button>
							<Button variant="danger" size="sm"
							onClick={ () => deleteToggle(product._id)}>
								Delete
							</Button>
						

						{
							(product.isActive === true) ?
								<Button variant="warning" size="sm"
								onClick={()=> archiveToggle(product._id, product.isActive)}>
									Disable
								</Button>
							:
								
								<Button variant="success" size="sm"
								onClick={ () => unarchiveToggle(product._id, product.isActive)}>
									Enable
								</Button>
								
						}
						</Fragment>

					</td>
				</tr>
			)
		})


		setProducts(productsArr)
	}, [productsData])

	const openEdit = (productId) => {
		fetch(`https://murmuring-atoll-05490.herokuapp.com/api/products/${productId}`,{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setImage(result.image)
			setProductId(result._id);
			setProductName(result.productName);
			setDescription(result.description);
			setPrice(result.price);
			setInventory(result.inventory)
		})

		setShowEdit(true);
	}

	const closeEdit = () => {

		setShowEdit(false);
		setProductName("")
		setDescription("")
		setPrice(0)
		setInventory(0)
	}



	const unarchiveToggle = (productId, isActive) => {
			fetch(`https://murmuring-atoll-05490.herokuapp.com/api/products/${productId}/activate`, {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				console.log(result)

				fetchData();
				if(result){
					Swal.fire({
						title: "Success",
						icon: "success",
						"text": "Product successfully unarchived"
					})
				} else {
					fetchData();
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						"text": "Please try again"
					})
				}
			})
		}

		const deleteToggle = (productId) => {
			fetch(`https://murmuring-atoll-05490.herokuapp.com/api/products/${productId}/delete`, {
				method: "DELETE",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				console.log(result)

				fetchData();
				if(result){
					Swal.fire({
						title: "Success",
						icon: "success",
						"text": "Product successfully deleted"
					})
				} else {
					fetchData();
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						"text": "Please try again"
					})
				}
			})
		}


	/*edit course function*/
	const editProduct = (e, productId) => {

		e.preventDefault()

		fetch(`https://murmuring-atoll-05490.herokuapp.com/api/products/${productId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				inventory: inventory,
				image: image
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			fetchData()

			if(typeof result !== "undefined"){
				// alert("success")

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course successfully updated!"
				})

				closeEdit();
			} else {
				
				fetchData()

				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`https://murmuring-atoll-05490.herokuapp.com/api/products/${productId}/deactivate`,{
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization" : `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if (result){
				fetchData();
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course successfully archived/unarchived"
				})
			}
			else {
				fetchData();
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	const addProduct = (e) => {
		e.preventDefault()

		fetch('https://murmuring-atoll-05490.herokuapp.com/api/products/addProduct', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productName : productName,
				description : description,
				price : price,
				inventory : inventory,
				image: image
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			if(result){
				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Successfully added"
				})

				setProductName("")
				setDescription("")
				setPrice("")
				setImage("")
				closeAdd();
			}
			else {
				fetchData()

				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	

	return(
		<Container>
			<div>
				<div className="d-flex justify-content-end mb-2">
					<Button variant="dark" onClick={openAdd}>Add New Product</Button>
				</div>
			</div>
			<Table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Remaining Stocks</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{/*display the courses*/}
					{products}
				</tbody>
			</Table>
		{/*Edit Course Modal*/}
		
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={ (e) => editProduct(e, productId) }>
					<Modal.Header>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="courseName">
							<Form.Label>Product name</Form.Label>
							<Form.Control
								type="text"
								value={productName}
								onChange={ (e)=> setProductName(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="text"
								value={description}
								onChange={ (e)=> setDescription(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={ (e)=> setPrice(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="inventory">
							<Form.Label>inventory</Form.Label>
							<Form.Control
								type="number"
								value={inventory}
								onChange={ (e)=> setInventory(e.target.value)}
							/>
						</Form.Group>
						<Form.Group controlId="image">
							<Form.Label>image link</Form.Label>
							<Form.Control
								type="text"
								value={inventory}
								onChange={ (e)=> setImage(e.target.value)}
							/>
						</Form.Group>

					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={(e) => addProduct(e)}>
					<Modal.Header>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group courseId="courseName">
							<Form.Label>Product Name</Form.Label>
							<Form.Control 
								type="text"
								value={productName}
								onChange={(e)=> setProductName(e.target.value)}
								/>
						</Form.Group>

						<Form.Group courseId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								value={description}
								onChange={(e)=> setDescription(e.target.value)}
								/>
						</Form.Group>

						<Form.Group courseId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number"
								value={price}
								onChange={(e)=> setPrice(e.target.value)}
								/>
						</Form.Group>

						<Form.Group courseId="inventory">
							<Form.Label>Inventory</Form.Label>
							<Form.Control 
								type="number"
								value={inventory}
								onChange={(e)=> setInventory(e.target.value)}
								/>
						</Form.Group>

						<Form.Group controlId="image">
							<Form.Label>image link</Form.Label>
							<Form.Control
								type="text"
								value={image}
								onChange={ (e)=> setImage(e.target.value)}
							/>
						</Form.Group>

					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>cancel</Button>
						<Button variant="success" onClick={addProduct}>add item</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</Container>
	)
}