import React, {useState, useEffect, Fragment} from 'react'

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Swal from 'sweetalert2';



export default function OrderView(props){

	const {ordersData, fetchData } = props;

	const [allOrders, setAllOrders] = useState([]);



	useEffect( () => {
		const ordersArr = ordersData.map( (orders) => {
			console.log(orders.itemsPurchased[0].productName)
			return(
			<tr key={orders._id}>
				<td>{orders.customerName}</td>
				<td>{orders.purchasedOn}</td>
				<td>{orders.customerAddress}</td>
				<td>{orders.itemsPurchased[0].productName}</td>
				<td>{orders.itemsPurchased[0].price}</td>
				<td>{orders.itemsPurchased[0]._id}</td>
			</tr>
			)
		})


		setAllOrders(ordersArr)
	}, [ordersData])

	

	return(
		<Container>
			<Table>
				<thead>
					<tr>
						<th>Customer Id</th>
						<th>Date of Purhcase</th>
						<th>Customer Address</th>
						<th>Product name</th>
						<th>Product price</th>
						<th>Invoice Id</th>
					</tr>
				</thead>
				<tbody>
					{/*display the courses*/}
					{allOrders}
				</tbody>
			</Table>

		</Container>
	)
}