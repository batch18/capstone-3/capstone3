import React, {useState, useEffect}from 'react'

import {Container, CardColumns} from 'react-bootstrap';

import Products from './Products'

export default function UserView({productsData}){


	const [products, setProducts] = useState([])

	useEffect( () =>{

		const productsArr = productsData.map( (products)=> {


			if(products.isActive === true){
				return <Products key={products._id} productsProp={products}/>
			}
			else{
				return null
			}
		})

		setProducts(productsArr)

	},[productsData])


	return(
		<Container fluid="true">
				{products}
		</Container>

	)
}